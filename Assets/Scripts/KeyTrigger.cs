﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyTrigger : MonoBehaviour
{
    // public Color col = new Color(0,0,0,0); Red Green Blue Alpha for the numbers

    
    private MeshRenderer rend;
    public KeyColour colour;

    //sets the colour of the key at the start
    //this needs to match the doors that have been added
    //else this key would be useless
    private void Awake()
    {
        rend = GetComponent<MeshRenderer>();
        switch (colour)
        {
            case KeyColour.red:
                rend.material.color = Color.red;
                break;
            case KeyColour.blue:
                rend.material.color = Color.blue;
                break;
            case KeyColour.green:
                rend.material.color = Color.green;
                break;
            case KeyColour.yellow:
                rend.material.color = Color.yellow;
                break;
            default:
                break;
        }
    }

    //when the player walks into the trigger area
    //add the key and remove the object from play
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            KeyRing keyRing = other.GetComponent<KeyRing>();//temp store 
            if (keyRing.PickupKey(colour) == true)
            {
                gameObject.SetActive(false);
            }
        }
    }
}

//allows for other places to use this for sharing the colour information.
public enum KeyColour { red, blue, green, yellow }
