﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPPController : MonoBehaviour
{
    //floats must have an 'f' when the number has a fraction of a whole eg 2.05f

    public float speed = 3.0f;
    public float sprintSpeed = 7.0f;
    public float jumpForce = 0.25f;
    public float gravity = 0.75f;

    private float currentSpeed = 0;
    private float velocity = 0;
    private CharacterController controller;
    private Vector3 motion;
   
    //allows us to access the character controller of our player
    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }


    // Sets the base speed
    void Start()
    {
        currentSpeed = speed; // initialise the speed the player will move at
    }


    // Every frame this will update a movement for the player. 
    //This includes base movement, strafing and jumping
    //sprinting is only allowed on ground but the speed will transfer after a jump or fall occurs
    void Update()
    {
        motion = Vector3.zero;

        if (controller.isGrounded == true)
        {
            velocity = -gravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space) == true)
            {
                velocity = jumpForce;
            }
            else if (Input.GetKeyDown(KeyCode.LeftShift) == true)
            {
                // this is for when you press down to activate sprint speed
                if (currentSpeed != sprintSpeed)
                {
                    currentSpeed = sprintSpeed;
                }
            }
            else if (Input.GetKeyUp(KeyCode.LeftShift) == true)
            {
                //when you release the key, set back to normal speed
                if (currentSpeed != speed)
                {
                    currentSpeed = speed;
                }
            }
        }
        else
        {
            velocity -= gravity * Time.deltaTime;
        }

        ApplyMovement();

    }


    /// <summary>
    /// Allows for movement on the Vertical and Horizontal plane as well and giving the jump a thrust value.
    /// Applys the movement so the player can move each frame.
    /// </summary>
    void ApplyMovement()
    {
        //this is the direction in local        key bindings                        
        motion += transform.forward * Input.GetAxisRaw("Vertical") * currentSpeed * Time.deltaTime;
        motion += transform.right * Input.GetAxisRaw("Horizontal") * currentSpeed * Time.deltaTime;
        motion.y += velocity;
        controller.Move(motion);
    }

}
