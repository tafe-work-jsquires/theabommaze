﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonMouse : MonoBehaviour
{
    public float sensitivityX = 2.5f;
    public float sensitivityY = 2.5f;
    public float drag = 1.5f;
    //public float sensitivity;

    private Transform character;
    private Vector2 mouseDir;//this will get the two Axis values
    private Vector2 smoothing;// this be used to modify the mouseDir 
    private Vector2 result;// this will be the final value that is used for the mouse movement 

    private void Awake()
    {
        character = transform.root; // root is for the first child

    }//Awake End


    //every frame the camera will be moved according to the mouse movements
    //it does this by using a set of quaternions that change the rotation of the camera
    void Update()
    {

        mouseDir = new Vector2(Input.GetAxisRaw("Mouse X") * sensitivityX, Input.GetAxisRaw("Mouse Y") * sensitivityY);//
        smoothing = Vector2.Lerp(smoothing, mouseDir, 1 / drag);//
        result += smoothing;//
        result.y = Mathf.Clamp(result.y, -80, 80);//This will limit the camera from going around and behind the player

        transform.localRotation = Quaternion.AngleAxis(-result.y, Vector3.right);

        character.rotation = Quaternion.AngleAxis(result.x, character.up);
    }//Update End


}