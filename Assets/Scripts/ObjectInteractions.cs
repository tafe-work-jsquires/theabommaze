﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectInteractions : MonoBehaviour
{
    //an abstract is a template, it is similar to a blueprint.
    //this place is pretty much just to show how to use inheritance.

    public abstract void Activate();// this pretty much means if any other class inherits from ObjectInteractions, it must have this function that returns void.

}
