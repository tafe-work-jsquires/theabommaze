﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{

    public float maxHealth = 250;
    //public Text healthTextx; // holds the variable for the health bar text
    //Ctrl + r + r = rename all related
    public float CurrentHealth { get; private set; } = 0; // autoproperty we can get the value but only set the value in here
    public bool deadPlayer = false;

    // Sets the health at the start of the game
    void Start()
    {
        CurrentHealth = maxHealth; // this is done to set the starting health, so that the max cannot be changed later
        //update health UI stuff
        UIManager.instance.UpdateHealth(this);
    }


    /// <summary>
    /// Changes the health of the player.
    /// This will stop the player from going below 0 and above the max
    /// When a player dies will will call PlayerDeath()
    /// </summary>
    /// <param name="damage"></param>
    public void TakeDamage(float damage) 
    {
        CurrentHealth -= damage; // changes the health each time it is called.
                                 

        if (CurrentHealth > maxHealth)
        {
            CurrentHealth = maxHealth;
            
        }//end if

        if (CurrentHealth <= 0)
        {
            CurrentHealth = 0;
            deadPlayer = true;
            PlayerDeath();
        }//end if

        UIManager.instance.UpdateHealth(this);


        //healthText.gameObject.SetActive(false);// this will set the game object to off when you die          
        //healthText.text = "Dead";
        //else { healthText.text = "Health: " + currentHealth + " / " + maxHealth }//end else 
        //healthBar.fillAmount = currentHealth / maxHealth; // update the health bar

    }//TakeDamage End


    /// <summary>
    /// Checks if the player has died
    /// if true boot to menu
    /// </summary>
    public void PlayerDeath()
    {
        if (deadPlayer == true)
        {
            SceneManager.LoadScene(0);
        }
    }

    
    void Update()
    {
        
    }
}
