﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : ObjectInteractions
{
    public bool locked = true;
    public KeyColour colour;
    private MeshRenderer rend;

    
    //When the object is loaded it will use the colour that is selected in the development scene
    //this colour is used to check against the players keyring. 
    //If the player has the correct colour in their ring it will open

    private void Awake()
    {
        rend = GetComponentInChildren<MeshRenderer>();
        switch (colour)
        {
            case KeyColour.red:
                rend.material.color = Color.red;
                break;
            case KeyColour.blue:
                rend.material.color = Color.blue;
                break;
            case KeyColour.green:
                rend.material.color = Color.green;
                break;
            case KeyColour.yellow:
                rend.material.color = Color.yellow;
                break;
            default:
                break;
        }
    }

    //dont forget to use override
    //when the door is interacted with it will trigger its animation
    public override void Activate()
    {
        if(locked == false)
        {        
                Animator targetAnim = GetComponent<Animator>();
                bool toggle = targetAnim.GetBool("Door Toggle");//this gets the Door Toggle trigger for the animation
                targetAnim.SetBool("Door Toggle", !toggle);//send back the opposite of the above lines in order to start the other half of the animation        
          
        }
    }

    /// <summary>
    /// Gets a bool to see if the door is locked. 
    /// Sometimes we may want the door to open without a key or have a colourless door open freely
    /// </summary>
    /// <param name="activate"></param>
    public void UnlockDoor(bool activate)
    {
        if (locked == true)
        {
            locked = false;
            //open
            if (activate == true)
            {
                Activate();
            }
        }

    }
}
